# 15.6

## Merged MR's

[List of Merged MR's in 15.6](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&milestone_title=15.6&author_username=jannik_lehmann)

## Highlights of 15.6

- [Remove End-of-Support analyzers from SAST config UI](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102691)
- [Remove refactor_code_quality_extension Feature Flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102447)
- [Add Codequality multiple findings indicator](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/99526)
