# 15.9

## Merged MR's

[List of Merged MR's in 15.9](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&milestone_title=15.9&author_username=jannik_lehmann)

## Highlights of 15.7

- [Enable Code Quality Inline Findings feature flag by default](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109201)
- Parental leave :shrug:

