# 15.8

## Merged MR's

[List of Merged MR's in 15.8](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&milestone_title=15.8&author_username=jannik_lehmann)

## Highlights of 15.8

- [Remove Code-quality counter badge when report is not loaded](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108536)
- [Add data to shouldCollapse method of MR Widget Extension](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108201)
- [Update CodeQuality MR Widget Message with no findings](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108008)
- [Align CodeQuality MR Widget neutral status icon](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/108453)
