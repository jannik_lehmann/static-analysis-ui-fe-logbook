# 15.4

## Merged MR's

[List of Merged MR's in 15.4](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&milestone_title=15.4&author_username=jannik_lehmann)

## Highlights of 15.4

- [Streamlined Sign-in Page was released](https://about.gitlab.com/releases/gitlab-com/#streamlined-sign-in-page)
- [Codequality Tabs Migration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/95899) closing out the last missing part of a larger refactoring Effort across Groups
- Progress on Code Quality Inline Findings [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/96472), [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/98200)
