# 15.11

## Merged MR's

[List of Merged MR's in 15.11 Jannik](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&milestone_title=15.11&author_username=jannik_lehmann)
[List of Merged MR's in 15.11 Dheeraj](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&milestone_title=15.11&author_username=djadmin)

## Highlights of 15.11

- [Improve error message for Security Features with alias usage in ci file](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/115675)


