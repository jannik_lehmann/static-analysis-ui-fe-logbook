# 15.10

## Merged MR's

[List of Merged MR's in 15.10 Jannik](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&milestone_title=15.10&author_username=jannik_lehmann)
[List of Merged MR's in 15.10 Dheeraj](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&milestone_title=15.10&author_username=djadmin)

## Highlights of 15.10

- [Enhance Security Configuration Error Handling introducing Userfacing error utils](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/112193)
- [Warn user about tokens in the comments](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/114621)
- [Improve visibility of SAST Analyzers banner](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113184)


