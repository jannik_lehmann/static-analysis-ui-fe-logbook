# 15.5

## Merged MR's

[List of Merged MR's in 15.5](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&milestone_title=15.5&author_username=jannik_lehmann)

## Highlights of 15.5

- :shrug: Nothing to see here since I was on Parental Leave in 15.5
