# 15.7

## Merged MR's

[List of Merged MR's in 15.7](https://gitlab.com/dashboard/merge_requests?scope=all&state=merged&milestone_title=15.7&author_username=jannik_lehmann)

## Highlights of 15.7

- [Add polling mechanism for Code Quality MR Widget](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/106719)
- [Add resolved Indicator to CodeQuality MR Widget](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/106571)
- [Refactor codeQuality transition to use vue's transition component](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/105280)
